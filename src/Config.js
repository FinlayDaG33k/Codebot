var sensitive = require('../sensitivedata/data.js'); // Some sensitive data nobody has to ever read
module.exports = {
  DISCORDSERVER: "",
  USERNAME: process.env.USERNAME || sensitive.USERNAME || "",
  TOKEN: process.env.TOKEN || sensitive.TOKEN || "",
  BOT_DEBUG_LEVEL: 0, // 0 for no debug, 1 for minimal, 2 for somewhat detailed, 3 for verbose
  EXIT_ON_ERROR: "false"
};
