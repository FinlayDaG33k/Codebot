module.exports = {
  name: "help",
  exec: function(data){
    if(typeof data === "undefined") return;
	  require("../main.js").codebot.doSay(data.Event,"```\r\n"
    + "Help: Displays this help file\r\n"
    + "```\r\n");
  }
};
