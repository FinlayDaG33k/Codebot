module.exports = {
  name: "unknown",
  exec: function(data){
    if(typeof data === "undefined") return;
	  require("../main.js").codebot.doSay(data.Event,"I do not understand your command, please try again.");
  }
};
