/*
*------------------------------------------------------------------------
* Define the doSay function
*------------------------------------------------------------------------
*
* Use this function to say something to the Discord channel.
* 
*/
module.exports = {
  doSay: function(evt,message){
    evt.channel.send(message);
  }
};
