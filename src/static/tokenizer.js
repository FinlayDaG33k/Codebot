/*
*------------------------------------------------------------------------
* Define the tokenizer function
*------------------------------------------------------------------------
*
* Use this function to tokenize inputs.
* This will create 4 tokens.
* Everything beyond the 4th will just be part of the 4th
* 
*/
module.exports = {
  tokenizer: function(input) {
    return (/^(\S+) (\S+) (\S+) (\S+) (.*)$/.exec(input) || []).slice(2, 6);
  }
};