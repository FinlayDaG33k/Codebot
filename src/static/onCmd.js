/*
*------------------------------------------------------------------------
* Define the onCmd function
*------------------------------------------------------------------------
*
* This is the function that will be ran when a command aimed at codebot
* is detected.
* 
*/
module.exports = {
  onCmd: function(cmd, data){
    try{
      switch(cmd.toLowerCase()) {
        case "help":
          require("../cmds/help.js").exec(data);
          break;
        default:
          require("../cmds/unknown.js").exec(data);
          break;
      }
    }catch(e){
      if(require("../cmds/unknown.js").codebot.Config.BOT_DEBUG_LEVEL >= 1){
        console.error(`[${self.formatTime()}] ${e.message}`);
      }
    }
  }
};