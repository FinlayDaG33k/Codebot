/*
*------------------------------------------------------------------------
* Import requires libraries
*------------------------------------------------------------------------
*
*/

const Discord = require('discord.js'); // Discord
const Client = new Discord.Client();


/*
*------------------------------------------------------------------------
* Define the bot function itself
*------------------------------------------------------------------------
*
*/
 
function Codebot(){
  console.log(`Starting Codebot v${process.env.npm_package_version}`);


  /*
  *------------------------------------------------------------------------
  * Define some variables
  *------------------------------------------------------------------------
  *
  */

  var self = this; // define self with 'this'
  self.Config = require('./Config'); // Load the config

  /*
  *------------------------------------------------------------------------
  * Load some functions from the files
  *------------------------------------------------------------------------
  *
  * Load functions here that shouldn't change too often (to keep this file
  * nice and compact)
  * 
  */

  self.doSay = require('./static/doSay').doSay;
  self.onCmd = require('./static/onCmd').onCmd;
  self.formatTime = require('./static/formatTime').formatTime;
  self.tokenizer = require('./static/tokenizer').tokenizer;

  
  /*
  *------------------------------------------------------------------------
  * Connect the bot to the Discord Websocket
  *------------------------------------------------------------------------
  * 
  */

  console.log(`[${self.formatTime()}] Connecting to Discord`);

  /* Connected to Discord */
  Client.on('ready', function (evt) {
    console.log(`[${self.formatTime()}] Connected to Discord as: ${Client.user.username}`);
  });

  Client.login(self.Config.TOKEN);

  /*
  *------------------------------------------------------------------------
  * Bot has seen a message fly by!
  *------------------------------------------------------------------------
  *
  * Check whether it's aimed at Codebot.
  * If so, do stuff with it.
  * Else, ignore it.
  * 
  */

  Client.on('message', message => {
    /* Check if the message is aimed at Codebot */
    if (message.content.indexOf('!codebot') == 0) {
       // It is aimed at Codebot!
       console.log(`[${self.formatTime()}] ${message.author.username}: ${message}`);

      /* Check whether there is a command or not */
      if(message.content.trim().indexOf(' ') != -1){
        // There is a command!
        var cmd = message.content.split(" ")[1].replace('!codebot',""), // Remove the !codebot word
				    user = user,
				    channelID = channelID,
            parameters = [];
            

        /* Load the parameters */
        for(var i=1; i<message.content.split(" ").length; i++){
          var parameter = message.content.split(" ")[i];
          parameters.push(parameter);
        }

        /* Of there are more than 4 parameters, tokenize the thing */
        if(parameters.length > 4){
          parameters = self.tokenizer(message.content);
        }
        
        /* Check whether there is a command, and if so, write it to the console */
        if(typeof cmd !== 'undefined'){
          console.log(`[${self.formatTime()}] ${message.author.username} issued command: \"${cmd}\" in server \"${message.guild.name}\"`); // write the command to the console
        }

        /* dump the parameters */
        if(self.Config.BOT_DEBUG_LEVEL >= 2){
          console.log(`[${self.formatTime()}] parameters given: ${parameters}`);
        }

        // check wether a command has been entered, and if so, process it
        if(typeof cmd !== 'undefined'){
          // process the command
          self.onCmd(cmd, {
            Client: Client,
            Event: message,
            Username: message.author.username,
            UID: message.author.id,
            CID: message.channel.id,
            GID: message.guild.id,
            Params: parameters
          });
        }
      }else{
        // There is no command :(
        /* Run the no_command.js from the cmds folder */
				require("./cmds/no_command.js").exec(message);
      }
    }
  });
}
module.exports.codebot = new Codebot();


/*
*------------------------------------------------------------------------
* Something went wrong!
*------------------------------------------------------------------------
*
* Check whether BOT_DEBUG_LEVEL is equal to or higher than 2;
* If so, write the error message and stacktrace to the console
*
* Check whether EXIT_ON_ERROR is true;
* If so, exit the bot
* 
*/

process.on('uncaughtException', function(err) {
	var self = this;
  self.Config = require('./Config'); // Get the configs

  // If the BOT_DEBUG in the Configs is set to true
	if(self.Config.BOT_DEBUG_LEVEL >= 2){
		console.error((new Date).toUTCString() + ' uncaughtException:', err.message);
		console.error(err.stack);
  }
  
  // If the EXIT_ON_ERROR in the Configs is set to true
  if(self.Config.EXIT_ON_ERROR == "true"){
		process.exit(1); // Exit the process
	}
});
